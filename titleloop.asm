titlescreen:
		bsr build_title
		move.b #$ff,titleflag
		lea (music1+40),a2
		move.l a2,vgm_start		
		move.b #$ff,rasterflag
		move.b #$01,page_ID
		move.w #$aa20,cursorpos		
		bsr new_page
		move.w #$2300,sr
titleloop:
		cmpi.b #$7f,d3
		 beq startgame
		cmpi.b #$3f,d3
		 beq load_to_editor	
		cmpi.b #$6f,d3
		 beq load_to_game		 
		;bsr music_driver
        bsr read_controller	
		bra titleloop
		
delay:
		nop
		dbf d4, delay
		rts
		
vblank_title:
        clr hblanks
		bsr calc_raster	
		bsr titlecontrol
		bsr music_driver				
		;move.w #$00ff,d4 ;compensate for shorter vblank (for sound tempo)
		;bsr delay		
		rte

titlecontrol:
		cmpi.b #$FB, d3
		beq pageleft
		cmpi.b #$07, page_ID
		 beq sound_test				
		cmpi.b #$F7, d3
		beq pageright
		rts
pageleft:
		cmpi.b #$01,page_ID
		beq return
		sub.b #$01,page_ID
		bsr new_page
		bra unhold
		;bsr unhold
		;rts
pageright:
		cmpi.b #$07,page_ID
		beq return
		add.b #$01,page_ID
		bsr new_page
		bra unhold		
		;bsr unhold		
		;rts
			
new_page:
		lea pages,a0		
		moveq #$00000000,d0
		move.b page_ID,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

pages:
	dc.w page1
	dc.w page2
	dc.w page3
	dc.w page4
	dc.w page5
	dc.w page6
	dc.w page7
	
page1:
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (title_text),a5
		bra smalltextloop
		;bsr smalltextloop
		;rts
page2:	
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (story_text),a5
		bra smalltextloop
		;bsr smalltextloop
		;rts	
page3:	
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (block_text),a5
		bra smalltextloop
		;bsr smalltextloop
		;rts		
page4:	
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (help_text),a5
		bra smalltextloop
		;bsr smalltextloop
		;rts
page5:	
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (credits_text),a5
		bra smalltextloop
		;bsr smalltextloop
		;rts
page6:
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (meta_text),a5
		bsr smalltextloop
		bra write_checksum
		;bsr write_checksum
		;rts
page7:
		move.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (sound_test_text),a5
		bra smalltextloop	
		;bsr smalltextloop	
		;rts
		
		
end_title:
		;move.b #$00,titleflag
		move.b #$05,vblank_ID
		;move.w #$2700,sr
		clr rasterflag
		lea (sprite_buffer+4),a5
		move.w (a5),d7
		andi.w #$2fff,d7	;flip pig
		move.w d7, (a5)		
		bra loop
		;rts
sound_test:	
		tst.b d3
		bne return
		bsr read_controller
		bsr soundtest_control
		moveq #$00000000,d5
		moveq #$00000000,d0
		move.w cursorpos,d0
		bsr calc_vram
		move.l d0,$4(a4)
		move.w #$403c,(a4)
		move.w #$402d,(a4)
		move.w #$402d,(a4)	
		moveq #$00000000,d0		
		move.w oldcursorpos,d0
		bsr calc_vram
		move.l d0,$4(a4)
		move.w #$0000,(a4)	
		move.w #$0000,(a4)	
		move.w #$0000,(a4)
		rts
soundtest_control:
		move.b d3,d7
		or.b #$Fe,d7
		cmpi.b #$Fe, d7
		beq cursorup	
		move.b d3,d7
		or.b #$Fd,d7		
		cmpi.b #$Fd, d7
		beq cursordown	
		rts
cursorup:
		cmpi.w #$A8A0,cursorpos	
		beq return
		move.w cursorpos,oldcursorpos	
		sub.w #$0080,cursorpos
		;bsr unhold
		bra newsound
		;bsr newsound
		;rts
cursordown:
		cmpi.w #$AC20,cursorpos	
		beq return
		move.w cursorpos,oldcursorpos
		add.w #$0080,cursorpos
		;bsr unhold	
		bra newsound		
		;bsr newsound		
		;rts
newsound:
		cmpi.w #$A8A0,cursorpos	
		beq load_music8
		cmpi.w #$A920,cursorpos	
		beq load_music7
		cmpi.w #$A9a0,cursorpos	
		beq load_music6
		cmpi.w #$AA20,cursorpos	
		beq load_music1
		cmpi.w #$AAa0,cursorpos	
		beq load_music2		
		cmpi.w #$AB20,cursorpos	
		beq load_music3	
		cmpi.w #$ABa0,cursorpos	
		beq load_music4
		cmpi.w #$AC20,cursorpos	
		beq load_music5				
		rts
		
load_music1:
		lea (music1+40),a2
		move.l a2,vgm_start	
		rts
load_music2:
		lea (music2+40),a2
		move.l a2,vgm_start	
		rts
load_music3:
		lea (music3+40),a2
		move.l a2,vgm_start	
		rts
load_music4:
		lea (music4+40),a2
		move.l a2,vgm_start	
		rts
load_music5:
		lea (music_end+40),a2
		move.l a2,vgm_start	
		rts
load_music6:
		lea (music6+40),a2
		move.l a2,vgm_start	
		rts		
load_music7:
		lea (music_gameoverOG+40),a2
		move.l a2,vgm_start	
		rts
load_music8:
		lea (music_ending+40),a2
		move.l a2,vgm_start	
		rts	

write_checksum:               ;one word
		move.l #$0000A92C,d0
		bsr calc_vram
		move.l d0,$4(a4)
		move.l checksum,d0
		bsr write_32
		
		move.l #$0000A8AC,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (game_checksum),a5
		move.l (a5),d0	
		bra write_32
		;bsr write_32
		;rts
		
write_32:		
		swap d0	
        moveq #$00000000, d5
	    move.l d0,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)

	    move.l d0,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)

	    move.l d0,d5
		andi.w #$00f0,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)

	    move.l d0,d5
		andi.w #$000f,d5		
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)
		
		swap d0
		
	    move.l d0,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)

	    move.l d0,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)

	    move.l d0,d5
		andi.w #$00f0,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)

	    move.l d0,d5
		andi.w #$000f,d5		
		andi.w #$00ff, d5
    	add.w #$0090,d5
		move.w d5,(a4)	
		rts		