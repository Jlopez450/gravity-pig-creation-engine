init_editor: ;run once
		move.b #$01,blockselectorpos
		move.b #$01,themeselectorpos
		move.b #$01,musicselectorpos	
	
init_editor_loading:	
		move.l #$70000000,$4(a4)
		move.w #$5000,d4
		bsr vram_clear_loop
		bsr clear_lvlbuffer
init_editor_resume:		
		move.b #$01,menu_num	
		clr playflag
		move.w #$8Aff,$4(a4)
		bsr newtheme			
		bsr newsong
		bsr newblock	
		lea (sprite_buffer),a5
		move.w #$0100,(a5)+
		move.w #$0001,(a5)+
		move.w #$00A7,(a5)+
		move.w #$0100,(a5)+
		
		move.w #$0080,(a5)+
		move.w #$0000,(a5)+
		move.w #$4056,(a5)+ ;pointer
		move.w #$00b8,(a5)+			
		rts
		
editloop:
		move.b #$ff,vb_flag
		bsr input_editor	
vb_wait:			
		tst.b vb_flag
		bne vb_wait	
		bra editloop		
		
editor: ;vblank vector
		movem.l d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a3/a5/a6, -(sp)
		clr vb_flag		
		bsr rotate_palette
		bsr check_menu
		bsr update_sprites_game
		bsr animate_coin
		bsr color_spikes		
		bsr music_driver		
		movem.l (sp)+, d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a3/a5/a6
		rte
		
settheme_mountain:
		nop	;experimental crash fix
		move.w sr,-(sp)
		nop ;ditto
		move.w #$2700,sr
		move.w #$8134,$4(a4)
		clr colorflag
		move.l #$50000000,$4(a4)		
		lea (level1tiles),a5
		move.w #$0600,d4
		bsr vram_loop	
		lea (palette),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background1), a5
		move.l #$70000000,$4(a4)
		move.w #$1960,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg1_map),a5
		bsr map_background	
		bsr newmenu		
		tst playflag
		bne skip		
		lea (sprite_buffer+14),a5
		move.w #$0108,(a5)	
skip:		
		move.w #$8174,$4(a4)
		move.w (sp)+,sr
		rts
settheme_ice:
		nop
		move.w sr,-(sp)
		nop
		move.w #$2700,sr
		move.w #$8134,$4(a4)
		clr colorflag
		move.l #$50000000,$4(a4)
		lea (level2tiles),a5
		move.w #$0600,d4
		bsr vram_loop	
		lea (palette2),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background2), a5
		move.l #$70000000,$4(a4)
		move.w #$3530,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg2_map),a5
		bsr map_background	
		bsr newmenu	
		tst playflag
		bne skip	
		lea (sprite_buffer+14),a5
		move.w #$0140,(a5)		
		move.w #$8174,$4(a4)
		move.w (sp)+,sr	
		rts	
settheme_tech:
		nop
		move.w sr,-(sp)
		nop
		move.w #$2700,sr
		move.w #$8134,$4(a4)
		move.b #$FF,colorflag
		move.w #$0CCC,spikecolor		
		move.l #$50000000,$4(a4)		
		lea (level3tiles),a5
		move.w #$0600,d4
		bsr vram_loop	
		lea (palette3),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background3), a5
		move.l #$70000000,$4(a4)
		move.w #$1010,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg3_map),a5
		bsr map_background	
		bsr newmenu	
		tst playflag
		bne skip	
		lea (sprite_buffer+14),a5
		move.w #$0178,(a5)				
		move.w #$8174,$4(a4)
		move.w (sp)+,sr
		rts		

input_editor:
		cmpi.b #$6f,d3
		beq unhold
		bsr read_controller
		cmpi.b #$6f,d3
		beq end_edit

		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		beq menu
		
		move.b d3,d7
		or.b #$Bf,d7
		cmpi.b #$Bf, d7
		beq place_block	
		
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq editor_left
		
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq editor_right
					
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq editor_up	
		
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq editor_down		
		rts
		
check_menu:
		tst menuflag
		bne return
		bsr read_controller
		move.b d3,d7
		or.b #$df,d7
		cmpi.b #$df, d7
		beq rotate_menu	
		rts
		
rotate_menu:	
		cmpi.b #$03,menu_num
		bge resetmenu
		add.b #$01,menu_num
		bra newmenu
		;bsr newmenu
		;rts
resetmenu:
		clr menu_num
		bra rotate_menu
newmenu:	
		cmpi.b #$02,vblank_ID ;fixes issue with loading straight to gameplay
		beq return
		bsr unhold
		lea menu_table,a0		
		moveq #$00000000,d0
		move.b menu_num,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing	
		jmp (a0)
		
menu_table:
 dc.w sethud_block
 dc.w sethud_theme
 dc.w sethud_music

sethud_block:
		lea (blocks),a5
		move.l #$40000003,$4(a4)		
		bsr overlay_items
		lea (Blocktxt),a5
		move.l #$40800003,$4(a4)
		bra overlay_text			
		;bsr overlay_text			
		;rts
sethud_theme:
		lea (themes),a5
		move.l #$40000003,$4(a4)		
		bra overlay_text	
		;bsr overlay_text	
		;rts
sethud_music:
		lea (songs),a5
		move.l #$40000003,$4(a4)		
		bra overlay_text	
		;bsr overlay_text	
		;rts		
		
menu:
		move.b #$ff,menuflag
		bsr unhold
		cmpi.b #$01,menu_num
		beq blockmenu
		cmpi.b #$02,menu_num
		beq thememenu
		cmpi.b #$03,menu_num
		beq musicmenu				
		rts
		
musicmenu:	
		lea (sprite_buffer+14),a5
		move.w #$0D0,(a5)
musicmenuloop:
		move.b #$ff,vb_flag
		bsr read_controller		
		bsr musicmenu_input
vb_wait4:			
		tst.b vb_flag		
		bne vb_wait4	
		bra musicmenuloop	

musicmenu_input:
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		beq nomenu

		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq music_left
		
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq music_right		
		rts	
		
music_left:
		cmpi.b #$01,musicselectorpos
		ble return
		sub.b #$01,musicselectorpos
		lea (sprite_buffer+14),a5
		sub.w #$0028,(a5)		
		bra newsong

music_right:
		cmpi.b #$06,musicselectorpos
		bge return
		add.b #$01,musicselectorpos
		lea (sprite_buffer+14),a5
		add.w #$0028,(a5)		
		bra newsong		
		
newsong:
		bsr editor_unhold
		lea music_table,a0		
		moveq #$00000000,d0
		move.b musicselectorpos,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing		
		jmp (a0)
		
music_table:
 dc.w set_music_1 ;Pig's song
 dc.w set_music_2 ;bassy tune
 dc.w set_music_3 ;icy tune
 dc.w set_music_4 ;hard reset
 dc.w set_music_5 ;ending theme
 dc.w set_music_6 ;mystic

set_music_1:
		lea (music1+40),a2
		move.l a2,vgm_start	
		tst playflag
		bne return
		lea (sprite_buffer+14),a5
		move.w #$00d0,(a5)
		rts
set_music_2:
		lea (music2+40),a2
		move.l a2,vgm_start
		tst playflag
		bne return	
		lea (sprite_buffer+14),a5
		move.w #$00f8,(a5)		
		rts
set_music_3:
		lea (music3+40),a2
		move.l a2,vgm_start	
		tst playflag
		bne return
		lea (sprite_buffer+14),a5
		move.w #$0120,(a5)
		rts
set_music_4:
		lea (music4+40),a2
		move.l a2,vgm_start	
		tst playflag
		bne return
		lea (sprite_buffer+14),a5
		move.w #$0148,(a5)		
		rts
set_music_5:
		lea (music_ending+40),a2
		move.l a2,vgm_start	
		tst playflag
		bne return
		lea (sprite_buffer+14),a5
		move.w #$0170,(a5)
		rts
set_music_6:
		lea (music6+40),a2
		move.l a2,vgm_start
		tst playflag
		bne return	
		lea (sprite_buffer+14),a5
		move.w #$0198,(a5)
		rts
 
 
thememenu:	
		lea (sprite_buffer+14),a5
		move.w #$0108,(a5)
thememenuloop:
		move.b #$ff,vb_flag	
		bsr read_controller		
		bsr thememenu_input
vb_wait3:			
		tst.b vb_flag		
		bne vb_wait3	
		bra thememenuloop	

thememenu_input:
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		beq nomenu

		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq theme_left
		
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq theme_right		
		rts	

theme_left:
		cmpi.b #$01,themeselectorpos
		ble return
		sub.b #$01,themeselectorpos
		lea (sprite_buffer+14),a5
		sub.w #$0038,(a5)		
		bra newtheme

theme_right:
		cmpi.b #$03,themeselectorpos
		bge return
		add.b #$01,themeselectorpos
		lea (sprite_buffer+14),a5
		add.w #$0038,(a5)		
		bra newtheme
		
newtheme:
		bsr editor_unhold
		lea theme_table,a0		
		moveq #$00000000,d0
		move.b themeselectorpos,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing		
		jmp (a0)

theme_table:
 dc.w settheme_mountain	
 dc.w settheme_ice	
 dc.w settheme_tech
		
blockmenu:
		bsr newblock
blockmenuloop:		
		move.b #$ff,vb_flag	
		bsr read_controller				
		bsr blockmenu_input
vb_wait2:			
		tst.b vb_flag		
		bne vb_wait2	
		bra blockmenuloop	
		
blockmenu_input:
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f, d7
		beq nomenu

		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq block_left
		
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq block_right		
		rts
nomenu:
		clr menuflag
		bsr unhold
		addq #$08,sp		;reset stack pointer and prevent overflow
		bra editloop
		
block_left:	
		cmpi.b #$01,blockselectorpos
		ble rotateleft
		sub.b #$01,blockselectorpos
		lea (sprite_buffer+14),a5
		sub.w #$0008,(a5)		
		bra newblock
rotateleft:
		move.b #$10,blockselectorpos	
		lea (sprite_buffer+14),a5
		move.w #$0130,(a5)
		bra newblock
		
block_right:
		cmpi.b #$10,blockselectorpos
		bge loopright
		add.b #$01,blockselectorpos	
		lea (sprite_buffer+14),a5
		add.w #$0008,(a5)
		bra newblock
loopright:
		move.b #$01,blockselectorpos	
		lea (sprite_buffer+14),a5
		move.w #$00B8,(a5)
		bra newblock
		
newblock:
		bsr editor_unhold
		lea block_table,a0		
		moveq #$00000000,d0
		move.b blockselectorpos,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing
		lea (sprite_buffer+4),a5		
		jmp (a0)				;crash here? Register conflict?

block_table:
 dc.w setblock1 ;grass up
 dc.w setblock2 ;grass down
 dc.w setblock3 ;spike up
 dc.w setblock4 ;spike down
 dc.w setblock5 ;goal top up
 dc.w setblock6 ;goal bottom up
 dc.w setblock7 ;goal top down
 dc.w setblock8 ;goal bottom down
 dc.w setblock9 ;wall
 dc.w setblocka ;dirt
 dc.w setblockb ;life
 dc.w setblockc ;coin
 dc.w setblockd ;gswap
 dc.w setblocke ;spike left
 dc.w setblockf ;spike right
 dc.w setblock10;eraser
 
setblock1:
		move.w #$00a7,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00B8,(a5)		;to auto-position cursor when switching menus
		rts	
setblock2:
		move.w #$00c7,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00c0,(a5)	
		rts	
setblock3:
		move.w #$00b3,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00c8,(a5)	
		rts	
setblock4:
		move.w #$00d3,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00d0,(a5)	
		rts	
setblock5:
		move.w #$00a9,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00d8,(a5)
		rts	
setblock6:
		move.w #$00c9,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00e0,(a5)			
		rts	
setblock7:
		move.w #$00a5,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00e8,(a5)			
		rts	
setblock8:
		move.w #$00c5,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00f0,(a5)			
		rts	
setblock9:
		move.w #$00b7,(a5)
		lea (sprite_buffer+14),a5
		move.w #$00f8,(a5)			
		rts	
setblocka:
		move.w #$00a4,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0100,(a5)			
		rts	
setblockb:
		move.w #$00ac,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0108,(a5)			
		rts	
setblockc:
		move.w #$00a2,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0110,(a5)			
		rts	
setblockd:
		move.w #$00b0,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0118,(a5)			
		rts			
setblocke:
		move.w #$00ab,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0120,(a5)			
		rts	
setblockf:
		move.w #$00cb,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0128,(a5)	
		rts	
setblock10:
		move.w #$00b8,(a5)
		lea (sprite_buffer+14),a5
		move.w #$0130,(a5)			
		rts			
		

place_block:
		lea (sprite_buffer+4),a5
		move.w (a5),d6	
		bsr calculate_placement
		andi.w #$00FF,d6
		cmpi.b #$b8,d6 ;special case (eraser)
		beq erase
		move.b d6,(a5)	
		
		move.l a5,d0
		sub.l #$00FF6000,d0
		lsl.w #$01,d0
		add.l #$0000a000,d0
		andi.w #$FFFE,d0		
		bsr calc_vram	
		move.l d0,$4(a4)
		move.w d6,(a4)		
		rts		
erase:	
		move.b #$00,(a5)
		move.l a5,d0
		sub.l #$00FF6000,d0
		lsl.w #$01,d0
		add.l #$0000a000,d0
		andi.w #$FFFE,d0		
		bsr calc_vram	
		move.l d0,$4(a4)
		move.w #$0000,(a4)				
		rts		
		
calculate_placement:
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		neg d7
		add.w d7,d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0080,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		rts
			
editor_left:
		lea (sprite_buffer+6),a5
		move.w (a5),d1
		cmpi.w #$0080,d1
		ble editor_scroll_level_left ;fixed the scroll issue
		sub.w #$08,d1
		move.w d1,(a5)
		bsr editor_scroll_level_left
		bra editor_unhold		
		;bsr editor_unhold		
		;rts
editor_scroll_level_left:	
		tst scroll_amount
		 beq return			
		add.w #$0008,scroll_amount
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)	
		lea (sprite_buffer+6),a5
		add.w #$0008,(a5)		
		rts		
		
editor_right:
		lea (sprite_buffer+6),a5
		move.w (a5),d1
		cmpi.w #$01b8,d1
		bge editor_scroll_level_right		
		add.w #$08,d1
		move.w d1,(a5)
		bsr editor_scroll_level_right
		bra editor_unhold
		;bsr editor_unhold
		;rts
editor_scroll_level_right:			
		cmpi.w #$FF40, scroll_amount
		 ble return	
		sub.w #$0008,scroll_amount
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)
		lea (sprite_buffer+6),a5
		sub.w #$0008,(a5)		
		rts	

editor_up:
		lea (sprite_buffer),a5
		move.w (a5),d1
		cmpi.w #$0090,d1
		ble return
		sub.w #$08,d1
		move.w d1,(a5)
		bra editor_unhold
		;bsr editor_unhold
		;rts		
		
editor_down:
		lea (sprite_buffer),a5
		move.w (a5),d1
		cmpi.w #$0158,d1
		bge return
		add.w #$08,d1
		move.w d1,(a5)
		bra editor_unhold
		;bsr editor_unhold
		;rts		

editor_unhold:
		bsr read_controller
		cmpi.b #$ff,d3		
		beq return
		or.b #$ef,d3
		cmpi.b #$ef,d3
		 beq return		 
		bra editor_unhold
	
stage_loop_editor:
		move.b (a6)+,d1	
		move.w d1,(a4)
		dbf d4, stage_loop_editor
		rts		
		
end_edit:
		clr scroll_amount
		move.l #$70000003,$4(a4);vram write $f000
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.b #$07,vblank_ID
		lea (sprite_buffer),a5
		move.w #$00B0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+			
placement_wait:
		cmpi.b #$03,vblank_ID		
		bne placement_wait
		move.w #$2700,sr		
		bsr savestage ;2nd last
		bsr build_userstage	
		move.w #$2300,sr			
		bra loop
		
placement: ;vblank vector
		bsr read_controller
		bsr placement_input
		bsr update_sprites_game
		rte
placement_input		
		move.b d3,d7
		;or.b #$7f,d7
		cmpi.b #$7F,d7
		beq end_placement		
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq placement_left	
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq placement_right					
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq placement_up		
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq placement_down			
		rts	
placement_left:	
		lea (sprite_buffer)+6,a5
		sub.w #$2,(a5)	
		rts
placement_right:
		lea (sprite_buffer)+6,a5
		add.w #$2,(a5)		
		rts
placement_up:
		lea (sprite_buffer),a5
		sub.w #$2,(a5)
		rts
placement_down:	
		lea (sprite_buffer),a5
		add.w #$2,(a5)
		rts
end_placement:
		lea (sprite_buffer)+8,a5
		move.w #$00b0,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w #$0100,(a5)+		
		move.b #$08,vblank_ID
		bra unhold
		; bsr unhold
		; rts
		
placement_crate: ;vblank vector
		bsr read_controller
		bsr placement_input_crate
		bsr update_sprites_game
		rte
placement_input_crate		
		move.b d3,d7
		;or.b #$7f,d7
		cmpi.b #$7F,d7
		beq end_placement_crate ;for real this time	
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq placement_left_crate
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq placement_right_crate
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq placement_up_crate
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq placement_down_crate	
		rts	
placement_left_crate:	
		lea (sprite_buffer)+14,a5
		sub.w #$2,(a5)	
		rts
placement_right_crate:
		lea (sprite_buffer)+14,a5
		add.w #$2,(a5)		
		rts
placement_up_crate:
		lea (sprite_buffer)+8,a5
		sub.w #$2,(a5)
		rts
placement_down_crate:	
		lea (sprite_buffer)+8,a5
		add.w #$2,(a5)
		rts
end_placement_crate:
		move.b #$03,vblank_ID
		rts		
		
savestage: ;Note to self: Finally works on real hardware, don't fuck with it anymore.
		lea ($200001),a5
		lea (level_buffer),a6
		move.w #$6FF,d4
		
		lea (sprite_buffer),a0
		move.w (a0),playerypos
		lea (sprite_buffer)+6,a0
		move.w (a0),playerxpos
		lea (sprite_buffer)+8,a0
		move.w (a0),crateypos
		lea (sprite_buffer)+14,a0
		move.w (a0),cratexpos		
		
		move.w playerxpos,d0
		andi.w #$FF00,d0
		lsr.w #$08,d0
		move.b d0,(a5)
		addq #2, a5 
		move.w playerxpos,d0
		andi.w #$00FF,d0	
		move.b d0,(a5)
		addq #2, a5 		
		
		move.w playerypos,d0
		andi.w #$FF00,d0
		lsr.w #$08,d0
		move.b d0,(a5)
		addq #2, a5 
		move.w playerypos,d0
		andi.w #$00FF,d0	
		move.b d0,(a5)
		addq #2, a5 		
		
		move.w cratexpos,d0
		andi.w #$FF00,d0
		lsr.w #$08,d0
		move.b d0,(a5)
		addq #2, a5 
		move.w cratexpos,d0
		andi.w #$00FF,d0	
		move.b d0,(a5)
		addq #2, a5 		
		
		move.w crateypos,d0
		andi.w #$FF00,d0
		lsr.w #$08,d0
		move.b d0,(a5)
		addq #2, a5 
		move.w crateypos,d0
		andi.w #$00FF,d0	
		move.b d0,(a5)
		addq #2, a5 		

		move.b ThemeSelectorpos,(a5)
		addq #2, a5 	
		
		move.b MusicSelectorpos,(a5)
		addq #2, a5 
		
saveloop:
		move.b (a6)+,(a5)
		addq #2, a5 
		dbf d4,saveloop			
		rts
		