Ram_start = $FF0000
level_buffer = $FF6000
Sprite_buffer = $FFFF00
vgm_start = $FF9000
grav_dir = $FFA000
scroll_amount = $FFA002
level_number = $FFA004
vblanks = $FFA006 
seconds = $FFA008  
minutes = $FFA00a 
region  = $FFA00c
lives  = $FFA00e
gswaps  = $FFA010
pcm_counter = $FFA012
hblanks = $FFA014
rastervblanks = $FFA016
rasterflag = $FFA018
direction = $FFA01a ;1=left 2=right
pushing = $FFA01c ;0=no 1=left 2=right
titleflag = $FFA01e
page_ID = $FFA020
cursorpos = $FFA022 ;for sound test
oldcursorpos = $FFA024 ;for sound test
bacon = $FFA026
total_bacon = $FFA028 ;reserved for good/bad ending
cutscene_ID = $FFA02A
oldcutscene_ID = $FFA02c
vertskewamount = $FFA02e
splashflag = $FFA030
anim_flag = $FFA032
vblank_ID = $FFA034
coinframe = $FFA036
cointimer = $FFA038
counter1 = $FFA03A
cshift = $FFA03C
speed = $FFA03E
random = $FFA040
wallpos = $FFA042
wallspeed = $FFA044
hitflag = $FFA046
removed = $FFA048
levelnum = $FFA04a
spikeflag = $FFA04c
spikedat = $FFA04E
spikedir = $FFA050 ;0=NA 1=falling down 2=up 3=right 4=left
colorflag = $FFA052 ;for color palette effects in last stages
spikerot = $FFA054
spikecolor = $FFA056
colorrotation = $FFA058 ;long
colorspeed = $FFA05C
text_timer = $FFA05E
stagebacon = $FFA060
skipflag = $FFA062

;editor/save specific stuff below this line
;----------------------------
sram_buffer = $FF5FF0 ;level buffer-$10
vb_flag = $FFA064
menu_num = $FFA066
BlockSelectorpos = $FFA068 ;block selection cursor
ThemeSelectorpos = $FFA070
MusicSelectorpos = $FFA072
menuflag = $FFA074

playerxpos = $FFA076
playerypos = $FFA078
cratexpos = $FFA07A
crateypos = $FFA07C

vb_flag = $FFA07E
playflag = $FFA080

;crash stuff
;----------------------------
checksum = $FFB000
D0_temp = $FFAF00
D1_temp = $FFAF04
D2_temp = $FFAF08
D3_temp = $FFAF0c
D4_temp = $FFAF10
D5_temp = $FFAF14
D6_temp = $FFAF18
D7_temp = $FFAF1c
A0_temp = $FFAF20
A1_temp = $FFAF24
A2_temp = $FFAF28
A3_temp = $FFAF2c
A4_temp = $FFAF30
A5_temp = $FFAF34
A6_temp = $FFAF38
A7_temp = $FFAF3c
sr_temp = $FFAF40
pc_temp = $FFAF44
errorID = $FFAF50
