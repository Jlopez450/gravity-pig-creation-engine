    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
 		move.w #$100,($A11100)
		move.w #$100,($A11200)   
		move.l #$FFFFF0,a7
		move.w #$2700, sr       ;disable ints
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		bsr region_check
		move.b #$01, level_number
		clr lives
		move.b #$04, vblank_ID

		;move.w #$FFC0, d7		
		move.w #$FFBE, d7		
		move.b #$ff,splashflag
		
		lea (splashscreen),a5
		;move.w #$4600,d4
		;move.l #$70000000,$4(a4)		
		;bsr vram_loop	
		lea (ram_start), a1
        bsr decompress			
		move.w #$9100,$4(a4)		;DMA length
		move.w #$9446,$4(a4) 
		move.w #$9500,$4(a4)        ;00
		move.w #$9600,$4(a4)        ;00
		move.w #$977f,$4(a4)        ;ff
		move.l #$70000084,$4(a4)				
		
		move.l #$40000003,$4(a4)	;vram write $c000	
		bsr generate_map
		
		lea (font),a5
		move.l #$40000000,$4(a4)
		move.w #$07FF,d4
		bsr vram_loop
		
		lea (level1tiles),a5
		move.w #$0600,d4
		bsr vram_loop		
		
		lea (palette_splash),a5
		move.w #$002f,d4
		move.l #$c0000000,$4(a4)
		bsr vram_loop
		move.w #$8230,$4(a4)
        move.w #$2300, sr       ;enable ints			
splashwait:
		nop
		tst splashflag
		bne splashwait
		move.w #$2700, sr	
		lea (splashtext),a5
		move.l #$0000cd00,d0
		bsr calc_vram
		move.l d0,$4(a4)
		bsr overlay_text
		move.w #$0001,d4	
 		move.w #$100,($A11100)
		move.w #$100,($A11200)   		
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000	
		lea (sfx_slogan),a5
		;move.w #$FFFF,d4		 ;comment out to skip pause
splashhold:
		bsr test2612
		move.b (a5)+,$A04001
		move.w #$000C,d0
		bsr delay22khz
		dbf d4,splashhold 
		bsr checksum_calc
		move.w #$8228,$4(a4)			
		
		lea (palette),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop
		
		move.l #$00001c00,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (playersprite),a5
		move.w #$013F,d4
		bsr vram_loop
        move.l #$40000010,$4(a4)   ;write to vsram    
		move.w #$0000,(a4)
		move.w #$0000,(a4)

		move.l #$c0000000,$4(a4)
		move.w #$0e80,(a4)
		move.l #$c0420000,$4(a4)   ;set sky blue title screen
		move.w #$0e80,(a4)
		
		bsr titlescreen	
        move.w #$2300, sr           ;enable ints		
		
loop:
		bsr calc_raster		
		bsr clock
		bsr animate_coin
		bsr control
		bsr gravity
		bsr check_crate		
		bsr check_right
		bsr right_collision
		bsr check_left	
		bsr left_collision
		bsr update_sprites_game
		bsr draw_hud
		bsr color_spikes
		bsr rotate_palette
		move.b #$ff,vb_flag	
main_wait:				
		;cmpi.b #$00,vb_flag
		;bne vb_wait
		tst.b vb_flag
		bne main_wait
		bra loop
		
delay22khz:
		nop
		dbf d0,delay22khz
		rts

setup_vdp:
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,$4(a4)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		moveq #$00000000,d1
		moveq #$00000000,d2
		moveq #$00000000,d3
		moveq #$00000000,d4
		moveq #$00000000,d5
		moveq #$00000000,d6
		moveq #$00000000,d7
		move.l d0, a0
		move.l d0, a1
		move.l d0, a2
		move.l d0, a3
		move.l d0, a4
		move.l d0, a5
		move.l d0, a6
		rts
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,$4(a4)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
clear_lvlbuffer:
		lea (level_buffer),a5
		move.w #$00700,d4
lvl_clear_loop:
		move.w #$0000,(a5)+
		dbf d4, lvl_clear_loop
		rts			
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts		

new_stage:
		move.w bacon,d0
		add.w d0,total_bacon ;add amount collected in level to total
		clr bacon  ;prep for next level	
		bra load_to_game

level_repeat:
		move.b #$01,level_number
		bra new_stage
nullstage:		
		add.b #$01,level_number
		bra new_stage
		
overlay_text:			;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4	;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$4000,d4
        move.w d4,(a4)		
		bra overlay_text
		
overlay_items:			;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4	;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		add.w #$0060,d4
        move.w d4,(a4)		
		bra overlay_items		
		
load_collision:
		move.w (a6)+,(a5)+
		dbf d4, load_collision
		rts
		
stage_loop:
		clr d1
		move.b (a6)+,d1	
		add.b #$60,d1
		move.w d1,(a4)
		dbf d4, stage_loop
		rts

text_loop:	
		move.b (a6)+,d1
		andi.w #$00ff,d1
		eor.w #$4000,d1
        move.w d1,(a4)	
		dbf d4, text_loop
		rts	
		
HBlank:
		move.l #$C07E0000,$4(a4)
		move.w cshift,(a4)

        add.w #$0001,hblanks	
		tst splashflag
		 bne titlescale
		 
		tst rasterflag
		 beq nowarp
		cmpi.w #$0010,hblanks
		ble nowarp
	    move.l #$70020003,$4(a4);vram write $f002   
	    move.w (a3)+,d5
		move.w d5,cshift		
	    lsr.w #$04,d5 			 ;intensity
	    move.w d5,(a4)		     ;write scroll data
warpret:		
update_dac:
		 tst.w pcm_counter
		 beq returnint
	    move.b #$2a,$A04000		
		sub.w #$0001,pcm_counter		
	    move.b (a1)+,$A04001
	    rte
update_dac_vb:		;for vblank		
		 tst.w pcm_counter
		 beq return
	    move.b #$2a,$A04000		
		sub.w #$0001,pcm_counter		
	    move.b (a1)+,$A04001
	    rts		

titlescale:
	   ;clr d0
	   ;clr d1
	   move.l #$50000010,$4(a4) ;write to VSRAM
	   move.w hblanks,d0    
	   mulu.w d7,d0	   		   
	   lsr #$06,d0	   
	   sub.w d7, d0   ;uncomment to center image
	   sub.w d7, d0   ;uncomment to center image
	   move.w d0,(a4)   
	   move.w d0,(a4)
	   rte	
nowarp:
	    move.l #$70020003,$4(a4);vram write $f002   
	    move.w #$0000,(a4)		     ;write scroll data		
		bra warpret	
		
Vblank:
		clr vb_flag
        clr hblanks
		add.w #$0001,counter1
		lea vblank_table,a0		
		moveq #$00000000,d0
		move.b vblank_ID,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)
		
vblank_table:
 dc.w vblank_title
 dc.w game 
 dc.w editor		;leftover from original game
 dc.w calc_script
 dc.w start
 dc.w game_over_vb		
 dc.w placement 
 dc.w placement_crate
		
game:
		move.w d4,-(sp)
		move.w d6,-(sp)
		bsr music_driver		
		move.w #$0014,d4		
		bsr dac			;improves quality by playing samples during VBLANK
		move.w (sp)+,d6
		move.w (sp)+,d4
        rte		
dac:	   
	   bsr update_dac_vb
	   nop   
	   nop
	   nop		;enough NOPs to roughly equal one HBLANK
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   nop
	   dbf d4, dac
	   rts	
			
rotate_palette:
		tst colorflag
		beq return
		add.b #$01,colorspeed
		cmpi.b #$24,colorspeed
		blt return
		clr colorspeed
		
		move.l #$C0080000,$4(a4)
		add.w #$01,colorrotation
		cmpi.w #$0008,colorrotation
		bgt resetcolor
colorret:		
		move.l a3,-(sp)
		lea (paletteloop),a3
		move.l a3,d0
		move.w colorrotation,d1
		add.l d1,d0
		add.l d1,d0
		move.l d0,a3
		move.w (a3),d0
		
		move.w d0,(a4)	
		move.l (sp)+,a3	
		rts
resetcolor:
		clr colorrotation
		bra colorret
		
color_spikes:	
		tst colorflag
		beq return
		move.l #$C0140000,$4(a4)
		move.w spikecolor,d0		
		add.b #$01,spikerot
		cmpi.b #$0A, spikerot	
		ble skipspikes
		eor.w #$0C00,d0
		move.w d0,spikecolor
		move.w d0,(a4)
		clr spikerot			
		rts	
skipspikes:	
		move.l #$C0140000,$4(a4)
		move.w d0,(a4)		
		rts
		   
calc_script:
	    clr hblanks 
	 	add.w #$0001,d7
		tst.w d7
		beq end_splash
		rte
end_splash:
		clr splashflag
		move.b #$01,vblank_ID
		rte
		
startgame:
		move.w #$2700,sr
		clr titleflag	
		move.b #$03,vblank_ID
		clr rasterflag
		bsr init_editor
		move.w #$2300,sr
		bra editloop
		;rte
		;rts
load_to_game:
		move.w #$2700,sr
		clr scroll_amount	
		move.l #$70000003,$4(a4);vram write $f000
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		clr titleflag	
		move.b #$02,vblank_ID
		clr rasterflag	
		lea ($200001),a5
		lea (level_buffer)-10,a6
		move.w #$7FF,d4
sram_load_loop_play:
		move.b (a5),(a6)+
		addq #2, a5 
		dbf d4,sram_load_loop_play	
		lea (level_buffer)-10,a6		
		move.w (a6)+,playerxpos
		move.w (a6)+,playerypos
		move.w (a6)+,cratexpos
		move.w (a6)+,crateypos
		move.b (a6)+,ThemeSelectorpos		
		move.b (a6)+,musicSelectorpos			
		bsr build_userstage
		
		lea (level_buffer),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop_editor
		
		move.w #$2300,sr			
		bra loop
		
load_to_editor:
		move.w #$2700,sr
		clr titleflag	
		move.b #$03,vblank_ID
		clr rasterflag	
		lea ($200001),a5
		lea (level_buffer)-10,a6
		move.w #$7FF,d4
sram_load_loop:
		move.b (a5),(a6)+
		addq #2, a5 
		dbf d4,sram_load_loop	
		
		lea (level_buffer)+128,a6 ;+128 to skip the HUD area
		move.l #$61000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop_editor	
		lea (level_buffer)-10,a6		
		move.w (a6)+,playerxpos
		move.w (a6)+,playerypos
		move.w (a6)+,cratexpos
		move.w (a6)+,crateypos
		move.b (a6)+,ThemeSelectorpos		
		move.b (a6)+,musicSelectorpos		
		move.b #$01,blockselectorpos			
		bsr init_editor_resume
		move.w #$2300,sr
		bra editloop	

animate_coin:
		add.b #$20,cointimer	;animate once ever 8 vblanks (7.5FPS)
		tst cointimer
		bne return	
		clr cointimer
		
anim_ret:	
		add.b #$01,coinframe
		cmpi.b #$09,coinframe
		bge loop_coin
		
		move.w #$000F,d4
		move.l #$54400000,$4(a4) ;vram write $1440
		
		lea cointable,a0		
		moveq #$00000000,d0
		move.b coinframe,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)				
loop_coin:
		clr coinframe
		bra anim_ret
 
cointable:
 dc.w ld_frame1
 dc.w ld_frame2
 dc.w ld_frame3
 dc.w ld_frame4
 dc.w ld_frame5 ;loop pattern (1,2,3,4,5,4,3,2)
 dc.w ld_frame4
 dc.w ld_frame3
 dc.w ld_frame2
 
ld_frame1:
		lea (coinframe1),a5
		bra vram_loop
ld_frame2:
		lea (coinframe2),a5
		bra vram_loop
ld_frame3:
		lea (coinframe3),a5
		bra vram_loop
ld_frame4:
		lea (coinframe4),a5
		bra vram_loop
ld_frame5:
		lea (coinframe5),a5
		bra vram_loop
		
check_crate:
		movem d3/d4/d5/d6, -(sp)	
		clr pushing			
		lea (Sprite_buffer),a5
		move.w (a5), d3 ;player ypos		
		lea (Sprite_buffer+6),a5		
		move.w (a5)+, d4	;player xpos	
		;lea (Sprite_buffer+8),a5			
		move.w (a5), d5 ;crate ypos		
		lea (Sprite_buffer+14),a5			
		move.w (a5), d6;crate xpos		
		cmpi.b #$01,direction
		 beq crateleft
		cmpi.b #$02,direction
		 beq crateright		 
		bra end_crate
end_crate:			
		movem (sp)+, d3/d4/d5/d6				
		rts
		
crateleft:	
		andi.w #$FFF8,d5	;make collision more liberal.
		andi.w #$FFF8,d3
        cmp.w d3,d5
		 blt end_crate ;d5 less than d3
		 
        cmp.w d3,d5
		 bgt end_crate

        cmp.w d4,d6
		 bgt end_crate	
		 
		sub.w #$0c,d4			  
        cmp.w d4,d6
		 blt end_crate
		 
		lea (Sprite_buffer+14),a5	
		sub.w #$0004,(a5)
		move.b #$01,pushing
		bra end_crate
		
crateright:
		andi.w #$FFF8,d5
		andi.w #$FFF8,d3
        cmp.w d3,d5
		 blt end_crate ;d5 less than d3
		 
        cmp.w d3,d5
		 bgt end_crate

        cmp.w d4,d6
		 blt end_crate
		 
		add.w #$1c,d4	
        cmp.w d4,d6
		 bgt end_crate	
		 
		lea (Sprite_buffer+14),a5	
		add.w #$0004,(a5)
		move.b #$02,pushing		
		bra end_crate	
			
calc_raster:
        add.w #$0002, rastervblanks	
	    cmpi.w #$0200,rastervblanks
	     bge reset_vb
	    lea (sine),a3
	    add.w rastervblanks,a3
	    rts
reset_vb:
	    clr rastervblanks
	    bra calc_raster		
	   
draw_hud:	
		tst cutscene_ID
		 bne return
		move.l #$408c0003,$4(a4)	;vram c08c	
        clr d0
		move.b minutes, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0 		
		move.w d0, (a4)
		;clr d0
		move.b minutes, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0 		
		move.w d0, (a4)	

		move.l #$40920003,$4(a4)	;vram c092
        ;clr d0
		move.b seconds, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0 		
		move.w d0, (a4)
		;clr d0
		move.b seconds, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0 		
		move.w d0, (a4)	

		move.l #$40a40003,$4(a4)	
        ;clr d0
		move.b lives, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0
		move.w d0, (a4)
		;clr d0
		move.b lives, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0
		move.w d0, (a4)

		move.l #$40b60003,$4(a4)
        ;clr d0
		move.b level_number, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0
		move.w d0, (a4)
		;clr d0
		move.b level_number, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0
		move.w d0, (a4)

		move.l #$40CA0003,$4(a4)
        ;clr d0
		move.b gswaps, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0
		move.w d0, (a4)
		;clr d0
		move.b gswaps, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		eor.w #$4000,d0
		move.w d0, (a4)		
		move.w #$0000, (a4)		
		rts
		
right_collision:
		cmpi.b #$AB,d5 ;spike
		beq kill

		cmpi.b #$C9,d5 ;exit
		beq get_goal
		cmpi.b #$a9,d5 ;exit
		beq get_goal
		cmpi.b #$C5,d5 ;exit
		beq get_goal	
		cmpi.b #$A5,d5 ;exit
		beq get_goal
		
		cmpi.b #$b7,d5 ;wall
		beq wall_right
		
		;cmpi.b #$a4,d5 ;dirt
		;beq wall_right
		;cmpi.b #$64,d5 ;dirt
		;beq wall_right			
		
		cmpi.b #$cc,d5;life
		beq addlife	
		cmpi.b #$ac,d5;life
		beq addlife	

		cmpi.b #$d0,d5 ;extra swap
		beq addswap	
		cmpi.b #$b0,d5 ;extra swap
		beq addswap	

		cmpi.b #$a2,d5 
		beq addbacon
		;cmpi.b #$62,d5
		;beq addbacon			
		rts
left_collision:
		cmpi.b #$CB,d5 ;spike
		beq kill

		cmpi.b #$C9,d5 ;exit
		beq get_goal
		cmpi.b #$a9,d5 ;exit
		beq get_goal
		cmpi.b #$C5,d5 ;exit
		beq get_goal	
		cmpi.b #$A5,d5 ;exit
		beq get_goal		
		cmpi.b #$b7,d5 ;wall
		beq wall_left
		
		;cmpi.b #$a4,d5 ;dirt
		;beq wall_left
		
		cmpi.b #$cc,d5;life
		beq addlife	
		cmpi.b #$ac,d5;life
		beq addlife	

		cmpi.b #$d0,d5 ;extra swap
		beq addswap	
		cmpi.b #$b0,d5 ;extra swap
		beq addswap	

		cmpi.b #$a2,d5
		beq addbacon
		;cmpi.b #$62,d5
		;beq addbacon			
		rts
		
addbacon:
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000		
		lea (sfx_coin),a1
		move.w #$0F10,pcm_counter

		add.w #$0001,bacon	
		moveq #$00000000,d0	
		move.l a5,d0
		move.b #$00,(a5) ;remove tile from collision
		sub.l #$00FF6000,d0
		lsl.w #$01,d0
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		move.w #$8000,(a4)
		rts		
		
addswap:
		add.b #$01,gswaps	
		moveq #$00000000,d0	
		move.l a5,d0
		move.b #$00,(a5) ;remove tile from collision
		sub.l #$00FF6000,d0
		lsl.w #$01,d0
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		move.w #$8000,(a4)
		rts
		
addlife:
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000		
		lea (sfx_life),a1
		move.w #$0FAA,pcm_counter
		
		clr d0
		clr d4
		move.b lives,d0
		move.b #$01,d4
		abcd d0,d4	
		move.b d4,lives		
		
		moveq #$00000000,d0	
		move.l a5,d0
		move.b #$00,(a5) ;remove tile from collision
		sub.l #$00FF6000,d0
		lsl.w #$01,d0
		add.l #$0000a000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		move.w #$0000,(a4)
		rts
				
wall_right:	
		lea (sprite_buffer+6),a5
		sub.w #$0002,(a5)
		add.w #$02,scroll_amount
		lea (sprite_buffer+14),a5 ;fixes flying crate when hitting wall issue
		add.w #$02,(a5)		
		rts
wall_left:		
		lea (sprite_buffer+6),a5
		add.w #$0002,(a5)
		sub.w #$02,scroll_amount	
		lea (sprite_buffer+14),a5 ;fixes flying crate when hitting wall issue
		sub.w #$0002,(a5)		
		rts
get_goal:
		clr d0
		clr d4
		move.b #$01,d0
		move.b level_number,d4
		abcd d0, d4
		move.b d4,level_number
		bra new_stage
		;bsr new_stage
		;rts
gravity:
		tst.b grav_dir
		 bne grav_up
		bra grav_down 
grav_down:
		bsr check_cratewalk
		bsr downgrav_pig
		bra downgrav_crate
		;bsr downgrav_crate
		;rts
		
check_cratewalk:
		movem d3/d4/d5/d6, -(sp)	
		lea (Sprite_buffer),a5
		move.w (a5), d3 ;player ypos		
		lea (Sprite_buffer+6),a5		
		move.w (a5)+, d4	;player xpos	
		;lea (Sprite_buffer+8),a5			
		move.w (a5), d5 ;crate ypos		
		lea (Sprite_buffer+14),a5			
		move.w (a5), d6;crate xpos	
		
		cmp.w d3,d5 ;D3 is less than D5
		ble end_crate
		
		add.w #$0012,d3 ;factor crate height
		cmp.w d3,d5
		bge end_crate
			
		add.w #$0008,d6
		cmp.w d6,d4
		bgt end_crate
		sub.w #$0008,d6		
	
		sub.w #$0010,d6
		cmp.w d4,d6
		bge end_crate
		add.w #$0010,d6

		lea (sprite_buffer),a5
		sub.w #$0003, (a5)
		movem (sp)+, d3/d4/d5/d6			
		rts	
check_cratewalk_reverse:
		movem d3/d4/d5/d6, -(sp)	
		lea (Sprite_buffer),a5
		move.w (a5), d3 ;player ypos		
		lea (Sprite_buffer+6),a5		
		move.w (a5)+, d4	;player xpos	
		;lea (Sprite_buffer+8),a5			
		move.w (a5), d5 ;crate ypos		
		lea (Sprite_buffer+14),a5			
		move.w (a5), d6;crate xpos	
		
		cmp.w d3,d5 ;D3 is less than D5
		bge end_crate
			
		sub.w #$0012,d3 ;factor crate height
		cmp.w d3,d5
	    ble end_crate
			
		add.w #$0008,d6
		cmp.w d6,d4
		bgt end_crate
		sub.w #$0008,d6		
	
		sub.w #$0010,d6
		cmp.w d4,d6
		bge end_crate
		add.w #$0010,d6

		lea (sprite_buffer),a5
		add.w #$0003, (a5)
		movem (sp)+, d3/d4/d5/d6			
		rts			
		
downgrav_crate:
		bsr check_floor_crate
		cmpi.b #$a7,d5 ;floor
		beq return		
		cmpi.b #$b3,d5 ;spike
		beq return	
		cmpi.b #$a4,d5 ;dirt
		beq return		
		lea (sprite_buffer+8),a5
		add.w #$0003,(a5) ;speed
		rts	
downgrav_pig:
		lea (sprite_buffer+4),a5
		move.w (a5),d7
		andi.w #$2fff,d7
		move.w d7, (a5)
		bsr check_floor

		cmpi.b #$a7,d5 ;floor
		beq return
		cmpi.b #$b3,d5 ;spike
		beq kill	
		
		cmpi.b #$b7,d5 ;wall
		beq return		
		
		cmpi.b #$ac,d5
		beq addlife	
		cmpi.b #$ac,d5
		beq addlife			
		cmpi.b #$d0,d5 ;extra swap
		beq addswap	
		cmpi.b #$b0,d5
		beq addswap	
		cmpi.b #$a2,d5
		beq addbacon
		lea (sprite_buffer),a5
		add.w #$0003,(a5) ;speed
		rts
grav_up:
		bsr check_cratewalk_reverse
		bsr upgrav_pig
		bra upgrav_crate
		;bsr upgrav_crate
		;rts
upgrav_crate:
		bsr check_roof_crate
		cmpi.b #$c7,d5 ;floor
		beq return	
		cmpi.b #$d3,d5 ;spike
		beq return
		cmpi.b #$a4,d5 ;dirt
		beq return	
		lea (sprite_buffer+8),a5
		sub.w #$0003,(a5) ;speed
		rts		
upgrav_pig:		
		lea (sprite_buffer+4),a5
		move.w (a5),d7
		or.w #$1000,d7
		move.w d7, (a5)
		bsr check_roof
		cmpi.b #$c7,d5 ;floor
		beq return
		cmpi.b #$d3,d5 ;spike
		beq kill	
		
		cmpi.b #$b7,d5 ;wall
		beq return		
			
		cmpi.b #$Ac,d5
		beq addlife	
		cmpi.b #$Ac,d5
		beq addlife			
		cmpi.b #$d0,d5 ;extra swap
		beq addswap	
		cmpi.b #$b0,d5
		beq addswap	
		cmpi.b #$a2,d5
		beq addbacon		
		
		lea (sprite_buffer),a5
		sub.w #$0003,(a5) ;speed
		rts
check_right:		
		;clr d0
		;clr d1
		
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		eor.w #$ffff,d7
		add.w d7,d0
		
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0060,d0
		sub.w #$0078,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1
	
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts

check_left:		
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		eor.w #$ffff,d7
		add.w d7,d0
		
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0078,d0
		sub.w #$0078,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1
	
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts		
		
check_floor:
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		eor.w #$ffff,d7
		add.w d7,d0
		
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0070,d0
		sub.w #$0070,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1
	
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts
check_floor_crate:
		lea (sprite_buffer+8),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+14),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		eor.w #$ffff,d7
		add.w d7,d0
		
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0070,d0
		sub.w #$0070,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1
	
		lea (level_buffer),a5
		add.l d1,a5
		subq #$01,a5
		move.b (a5),d5
		rts
		
check_roof:
		lea (sprite_buffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+6),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		eor.w #$ffff,d7
		add.w d7,d0
		
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0070,d0
		sub.w #$0080,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1
	
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts		
check_roof_crate:
		lea (sprite_buffer+8),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (sprite_buffer+14),a5
		move.w (a5),d0   ;X pos is now in d0
		move.w scroll_amount,d7
		eor.w #$ffff,d7
		add.w d7,d0
		
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0070,d0
		sub.w #$0080,d1
		lsl.w #$03,d1
		lsr.w #$03,d0
		add.w d0,d1

		lea (level_buffer),a5
		add.l d1,a5
		subq #$01,a5
		move.b (a5),d5
		rts

update_sprites_game: ;optimisation
		lea (sprite_buffer),a5
		move.l #$78000003,$4(a4) ;vram write $F800 (sprite)
		move.w #$0008,d4	
sprite_loop:
		move.w (a5)+,(a4)
		dbf d4, sprite_loop
		rts		
update_sprites:	
		lea (sprite_buffer),a5
		move.l #$78000003,$4(a4) ;vram write $F800 (sprite)
		move.w #$0060,d4
		bra sprite_loop
		
control:
		or.b #$bf,d3
		cmpi.b #$bf,d3
		 beq no_input	;fixes the flip spam issue
		bsr read_controller
		
		cmpi.b #$4F, d3
		beq suicide		;B+C+START for suicide		
		
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf, d7
		beq reverse_gravity
gravret:		
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq left
		
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq right				
		rts
no_input:
		bsr read_controller		
		bra gravret ;fixes the "gravity stun-lock" issue
left:
		move.b #$01,direction
		lea (sprite_buffer+6),a5
		;clr d1
		move.w (a5),d1
		sub.w #$02,d1
		move.w d1,(a5)
		lea (sprite_buffer+4),a5
		move.w (a5),d1
		or.w #$0800,d1
		move.w d1, (a5)
		bsr scroll_level_left
		bsr animate_player
		rts
scroll_level_left:	
		tst.w scroll_amount
		 beq return		
		
		add.w #$0002,scroll_amount
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)
		
		lea (sprite_buffer+14),a5
		add.w #$0002,(a5)
		rts
		
right:
		move.b #$02,direction
		lea (sprite_buffer+6),a5
		;clr d1
		move.w (a5),d1
		add.w #$02,d1
		move.w d1,(a5)
		lea (sprite_buffer+4),a5
		move.w (a5),d1
		andi.w #$F0FF,d1
		move.w d1, (a5)	
		bsr scroll_level_right	
		bsr animate_player		
		rts	
scroll_level_right:			
		cmpi.w #$FF40, scroll_amount
		 ble return	
		
		sub.w #$0002,scroll_amount
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)
		 
		lea (sprite_buffer+14),a5
		sub.w #$0002,(a5)			
		rts
animate_player:
		eor.b #$ff,anim_flag
		tst anim_flag
		beq return				;only animate every other vblank (30 times per second)
		lea (sprite_buffer+4),a5
		move.w (a5),d0
		move.w d0,d1
		andi.w #$00FF,d0
		andi.w #$FF00,d1
		cmpi.w #$00e0,d0
		beq setframe1
		eor.w #$00e0,d1
		move.w d1,(a5)	
		rts
setframe1:
		eor.w #$00e8,d1
		move.w d1,(a5)	
		rts	
up:
		lea (sprite_buffer),a5
		;clr d1
		move.w (a5),d1
		sub.w #$02,d1
		move.w d1,(a5)
		rts
down:
		lea (sprite_buffer),a5
		;clr d1
		move.w (a5),d1
		add.w #$02,d1
		move.w d1,(a5)
		rts	
reverse_gravity:
		;cmpi.b #$00,gswaps
		tst gswaps
		 beq gravret
		 ;beq return
		eor.b #$ff,grav_dir
		sub.b #$01,gswaps
		eor.b #$ff,rasterflag	
		bra gravret
		;rts
suicide:	
		bsr unhold
		bra kill
kill:
	    move.b #$2b,$A04000         ;DAC enable register             
	    move.b #$80,$A04001         ;enable DAC	
	    move.b #$2a,$A04000		
		lea (sfx_kill),a1
		move.w #$5630,pcm_counter
		clr rasterflag
		move.w #$2700,sr
killwait:
		bsr update_dac_vb
		move.w #$0014,d0
		bsr delay22khz
		tst pcm_counter
		bne killwait
		move.w #$2700,sr
		tst lives
		beq game_over
		clr d0
		clr d4
		move.b lives,d0
		move.b #$01,d4
		sbcd d4,d0	
		move.b d0,lives	
		clr bacon	
		clr scroll_amount	
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)		
		bra load_to_game
game_over:
		move.w #$2700,sr
		
		lea (game_over_text),a5
		moveq #$00000000,d0
		move.w scroll_amount,d0
		eor.w #$FFFF,d0		
		add.w #$0080,d0
		lsr.w #$03,d0
		add.l #$0000aa1E,d0
		andi.w #$FFFE,d0			
		bsr calc_vram
		move.l d0,$4(a4)		
		bsr overlay_text			
		lea (game_over_text2),a5
		moveq #$00000000,d0
		move.w scroll_amount,d0
		eor.w #$FFFF,d0		
		add.w #$0080,d0
		lsr.w #$03,d0		
		add.l #$0000aa90,d0
		andi.w #$FFFE,d0		
		bsr calc_vram
		move.l d0,$4(a4)	
		bsr overlay_text						
	    move.b #$2b,$A04000       
	    move.b #$80,$A04001	
	    move.b #$2a,$A04000
		move.b #$80,$A04001 ;center DAC
		lea (music_gameover+40),a2
		move.l music_mute,vgm_start	
		move.w #$0a80,d4
		clr pcm_counter
		move.b #$06,vblank_id
		clr spikedir
		clr rasterflag
		move.w #$2300,sr
		
game_over_loop:
		bra game_over_loop	

game_over_VB:
		bsr read_controller
		cmpi.b #$7f,d3
		beq no_tmss
		bsr music_driver
		rte
		
unhold:
		bsr read_controller
		cmpi.b #$ff,d3
		 beq return
		bra unhold	

read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts		
hex2dec:		
		move.w d0, -(sp)
		move.w d1, -(sp)	;push registers
		move.w d3, -(sp)
		move.w #$0001,d0
		clr d1
		move.w d7,d3
		clr d7
		sub.w #$0001,d3		;Set BCD adder to 1
hex2decloop:
		cmpi.b #$99,d1		;are we overflowing?
		beq bcdoverflow	
		abcd d0,d1			;add binary coded decimal
bcd_ret:		
		dbf d3, hex2decloop	
		bra end_bcd
bcdoverflow:
		abcd d0,d7			;count number of times we hit 99
		clr d1	;clear first counter
		bra bcd_ret
end_bcd:
		lsl.w #$08,d7		;make some room for 8 bits
		eor.w d1,d7 		;combine overflow register with main register
		move.w (sp)+,d0		
		move.w (sp)+,d1		;pop registers
		move.w (sp)+,d3	
		rts	
	
return:
		rts
returnint:
		rte
crash: ;halt and catch fire
		jmp $0 
	
generate_map:                  ;generate a map for 320x224 images
        move.w #$001c,d5
        move.l #$00008180,d0         ;first tile		
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$00,(a4)
        dbf d4,maploop2
        dbf d5,superloop
        rts	

smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return	
		cmpi.b #$20,d5
		 beq clearspace			;band-aid fix
clear_return:		 
		andi.w #$00ff,d5
		or.w #$4000,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
clearspace:
		move.b #$00,d5
		bra clear_return
	
build_title:
		move.w #$8134,$4(a4)
		lea (music4+40),a2
		move.l a2,vgm_start
		clr scroll_amount
		clr gswaps	
		clr rasterflag		
		clr grav_dir
		
		lea (sprite_buffer),a5
		move.w #$0082,(a5)+
		move.w #$0D01,(a5)+
		move.w #$28E0,(a5)+
		move.w #$0190,(a5)+		
		bsr clearcrate
		bsr update_sprites
		lea (title_level),a6
		move.l #$40000003,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		move.w #$8174,$4(a4)
		rts 
build_userstage:
		move.w #$8134,$4(a4)
		move.w #$8A00,$4(a4)
		clr scroll_amount
		clr gswaps	
		clr rasterflag		
		clr grav_dir
		
		lea (sprite_buffer),a5
		move.w playerypos,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w playerxpos,(a5)+	
		
		move.w crateypos,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w cratexpos,(a5)+	
		bsr update_sprites
		move.b #$ff,playflag
		bsr newtheme
		bsr newsong
		bsr set_hud		
		move.w #$8174,$4(a4)
		move.b #$02,vblank_ID
		rts 		
		
clearcrate:	
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		rts	
		
map_background:
		clr d7
map_backgroundloop:
		move.w (a5)+,d7	
		add.w #$6180,d7
		move.w d7,(a4)
		dbf d4,map_backgroundloop
		rts	
set_hud:
		move.l #$0000C000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (hud),a6
		move.w #$0070,d4
		bra text_loop
		
checksum_calc:			;Fuck your standard 16 bit checksum routine!
		move.l #$00000200, a0
		lea ROM_End, a1
		move.l a0,d0
		move.l a1,d1
		sub.l d0,d1
		lsr.w #$01,d1
checksum_loop:
		add.w (a0)+,d0	
		swap d0
		ror.l #$04,d0		
		dbf d1, checksum_loop
		move.l d0,checksum
        rts		
		
    include "editor.asm"
    include "titleloop.asm"
    include "clock.asm"
    include "decompress.asm"
    include "music_driver_V2.asm"
    include "crashscreen.asm"
	include "data.asm"
	
 dc.b "Scroll up, you wanker. There isn't anything else left!"	
ROM_End:       